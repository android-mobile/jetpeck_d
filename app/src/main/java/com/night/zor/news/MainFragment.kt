package com.night.zor.news

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.night.zor.R
import com.night.zor.databinding.FragmentMainBinding
import com.night.zor.viewmodel.MainViewModel

/**
 * @author 刘守康
 * 创建日期：3/17/21
 * 描述：DemoTestListActivity
 */
class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: ListAdapter;
    private lateinit var dataBinding: FragmentMainBinding;

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = ListAdapter(layoutInflater, requireContext())
        dataBinding.recyclerView.adapter = adapter;
        viewModel = ViewModelProvider(activity as ViewModelStoreOwner)[MainViewModel::class.java]
        viewModel.mainDataLiveData.observe(viewLifecycleOwner, {
            adapter.setDataList(it)

        })
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        return dataBinding.root
    }
}