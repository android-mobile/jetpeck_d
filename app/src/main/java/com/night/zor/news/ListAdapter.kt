package com.night.zor.news

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.night.zor.R
import com.night.zor.model.db.MainData

/**
 * @author 刘守康
 * 创建日期：3/17/21
 * 描述：ListAdapter
 */
class ListAdapter(
    mLayoutInflater: LayoutInflater,
    mContext: Context
) : BaseBindAdapter<MainData>(mLayoutInflater, mContext) {


    override fun getLayoutResId(): Int = R.layout.item_fragment_main
}