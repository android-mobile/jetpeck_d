package com.night.zor.news

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.night.zor.BR
import com.night.zor.R

/**
 * @author 刘守康
 * 创建日期：3/17/21
 * 描述：BaseBindAdapter
 */
abstract class BaseBindAdapter<T>(
    private val mLayoutInflater: LayoutInflater,
    val mContext: Context
) :
    RecyclerView.Adapter<BaseBindAdapter.Companion.MyViewHolder>() {

    private var mDataList: List<T>? = null

    companion object {
        class MyViewHolder(var dataBinding: ViewDataBinding) :
            RecyclerView.ViewHolder(dataBinding.root)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            DataBindingUtil
                .inflate(
                    mLayoutInflater,
                    getLayoutResId(),
                    parent,
                    false
                )
        )
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.dataBinding.setVariable(BR.model, getItem(position))
        holder.dataBinding.executePendingBindings()
        onBind(holder, getItem(position), position);
    }

    override fun getItemCount(): Int = mDataList?.size ?: 0


    private fun getItem(position: Int): T? {
        return if (position >= mDataList!!.size) {
            null
        } else mDataList!![position]
    }

    fun setDataList(dataList: List<T>) {
        mDataList = dataList
        notifyDataSetChanged()
    }

    @LayoutRes
    abstract fun getLayoutResId(): Int

    open fun onBind(holder: MyViewHolder, data: T?, position: Int) {}
}