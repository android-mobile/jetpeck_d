package com.night.zor.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.night.zor.model.data.IRepository
import com.night.zor.model.data.Repository
import com.night.zor.model.db.AppDatabase
import com.night.zor.model.db.MainData

/**
 * @author 刘守康
 * 创建日期：3/16/21
 * 描述：MainViewModel
 */
class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: IRepository = Repository()
    var mainDataLiveData: LiveData<List<MainData>> = MutableLiveData()

    init {
        mainDataLiveData = repository.select()
    }


    val stringLivaData: MutableLiveData<String> = MutableLiveData()
    private val words = arrayListOf(
        "你好",
        "哈哈哈哈哈哈",
        "算了算了",
        "出发",
        "春天来了",
        "持仓全在地板上，咋割呀",
        "不是本能哈",
        "狼来了",
        "再来一波这样跌，就可以考虑进场",
        "那个也仅有那么一天梭中了",
        "总共40W",
        "大三千会升仙",
        "小龙虾",
        "韭零后",
        "昨天全梭哈了",
    )


    /**
     * 获取数据
     */
    fun getTextList() {
        mainDataLiveData = repository.select()
    }


    /**
     * 掺入数据
     */
    suspend fun putTextContent() {
        val r = (0 until words.size).random()
        repository.insert(MainData(0, words[r]))
    }
}