package com.night.zor.model.data

import androidx.lifecycle.LiveData
import com.night.zor.model.db.MainData

/**
 * @author 刘守康
 * 创建日期：3/16/21
 * 描述：IRepository
 */
interface IRepository {

    suspend fun insert(mainData: MainData)

    fun select(): LiveData<List<MainData>>
}