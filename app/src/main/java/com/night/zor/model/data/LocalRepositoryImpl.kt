package com.night.zor.model.data

import androidx.lifecycle.LiveData
import com.night.zor.model.db.AppDatabase
import com.night.zor.model.db.MainData

/**
 * @author 刘守康
 * 创建日期：3/16/21
 * 描述：LocalRepositoryImpl
 */
class LocalRepositoryImpl : IRepository {

    override suspend fun insert(mainData: MainData) {
        AppDatabase.INSTANCE!!.mainDao().insertMainData(mainData)
    }

    override fun select(): LiveData<List<MainData>> {
        return AppDatabase.INSTANCE!!.mainDao().selectMainDataList()
    }
}