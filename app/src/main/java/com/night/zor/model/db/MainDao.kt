package com.night.zor.model.db

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

/**
 * @author 刘守康
 * 创建日期：3/16/21
 * 描述：MainDao
 */
@Dao
interface MainDao {
    @Insert
    fun insertMainData(vararg data: MainData)

    @Query("SELECT * FROM MainData order by id desc")
    fun selectMainDataList(): LiveData<List<MainData>>


}