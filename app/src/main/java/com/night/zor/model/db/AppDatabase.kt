package com.night.zor.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 *  author : night
 *  date   : 2020/11/11
 */
@Database(
    entities = [MainData::class], version = 1, exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun mainDao(): MainDao

    companion object {
        private val sLock = Any()
        var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase? {
            synchronized(sLock) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "main.db"
                    )
                        .build()
                }
                return INSTANCE
            }
        }
    }
}