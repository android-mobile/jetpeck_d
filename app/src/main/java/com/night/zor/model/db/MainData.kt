package com.night.zor.model.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author 刘守康
 * 创建日期：3/16/21
 * 描述：MainLivaData
 */
@Entity
data class MainData(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0, @ColumnInfo(name = "content") var content: String?
)
