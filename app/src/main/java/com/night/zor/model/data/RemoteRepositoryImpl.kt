package com.night.zor.model.data

import androidx.lifecycle.LiveData
import com.night.zor.model.db.MainData

/**
 * @author 刘守康
 * 创建日期：3/16/21
 * 描述：RemoteRepositoryImpl
 */
class RemoteRepositoryImpl : IRepository {
    override fun select(): LiveData<List<MainData>> {
        TODO("Not yet implemented")
    }

    override suspend fun insert(mainData: MainData) {
        TODO("Not yet implemented")
    }
}