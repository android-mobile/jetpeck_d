package com.night.zor.model.data

import androidx.lifecycle.LiveData
import com.night.zor.model.db.MainData

/**
 * @author 刘守康
 * 创建日期：3/16/21
 * 描述：Repository
 */
class Repository : IRepository {

    private val mRemoteRepository: IRepository = RemoteRepositoryImpl()
    private val mLocalRepository: IRepository = LocalRepositoryImpl()

    override suspend fun insert(mainData: MainData) {
        mLocalRepository.insert(mainData)
    }

    override fun select(): LiveData<List<MainData>> {
        return mLocalRepository.select()
    }
}