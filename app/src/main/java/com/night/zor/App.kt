package com.night.zor

import android.app.Application
import com.night.zor.model.db.AppDatabase

/**
 * author : night
 * date   : 2020/11/12
 */
internal class App : Application() {

    companion object{

    }
    override fun onCreate() {
        super.onCreate()
        AppDatabase.getInstance(this)
    }
}